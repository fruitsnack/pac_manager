# pac_manager

A script for declarative package management on arch-based distros and
automation of some common tasks.

## Requirements

- pacman
- yay

## Usage

Consult script's --help output for usage.  
Warning: this script doesn't support groups, only package names in lists. 

The script also checks *only* for explicitly installed packages.
If some packages are displayed as "missing" even though they are installed
most likely they're installed as dependency. They can be converted to
explicitly installed by:  
`sudo pacman -D --asexplicit pkgs`  

Example:  
`pac_manager -i -r ~/XFCE_List.md ~/XFCE_AUR_List.md`

## License
This script is licensed under coffeeware (fork of beerware) license.
